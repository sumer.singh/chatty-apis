<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\CustomerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// Admin
Route::post('/login', [AuthenticatedSessionController::class, 'store'])->middleware('guest');

// Customer
Route::post('customer-register',[CustomerController::class, 'userRegister']);
Route::post('customer-login',[CustomerController::class, 'userLogin']);
Route::get('customer-profile/{id}',[CustomerController::class, 'customerDetails']);
Route::get('customer-profile',[CustomerController::class, 'customerDetails']);
